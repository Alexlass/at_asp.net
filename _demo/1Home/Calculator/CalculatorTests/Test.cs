﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Calculator;

namespace CalculatorTests
{
    [TestFixture]
    public class Test
    {
        private Calculator.Calculator calc;

        [SetUp]
        public void SetUp()
        {
            calc = new Calculator.Calculator();
        }
    
        [Test]
        public void TestSum_5plus5_result10()
        {
            int result = 10;
            int a = 5;
            int b = 5;
            int methodResult = calc.Sum(a, b);
            Assert.AreEqual(result, methodResult);
        }

        [Test]
        public void TestDiff_7minus5_result2()
        {
            int result = 2;
            int a = 7;
            int b = 5;
            int methodResult = calc.Diff(a, b);
            Assert.AreEqual(result, methodResult);
        }

        [Test]
        public void TestMultiply_3mult3_result9()
        {
            int result = 9;
            int a = 3;
            int b = 3;
            int methodResult = calc.Multiply(a, b);
            Assert.AreEqual(result, methodResult);
        }

        [Test]
        public void TestDiv_9div3_result3()
        {
            double result = 3;
            int a = 9;
            int b = 3;
            double methodResult = calc.Div(a, b);
            Assert.AreEqual(result, methodResult);
        }
    }
}
