﻿using System.Collections.Generic;
using MyCalcLib;

namespace MyCalcLibTests
{
    class MockSequence : ISequence
    {
        public List<int> Values { get; set; }

        public List<int> NextInts()
        {
            return Values;
        }
    }
}
