﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_TASK1
{
    class Program
    {
        static void Main(string[] args)
        {
            Something somethingNull = new Something();
            Something something = new Something(1,2,3);
            Console.WriteLine(somethingNull);
            Console.WriteLine(something);
            Console.ReadKey();
        }
    }
}
